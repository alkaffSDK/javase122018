package basics2.completed;

import java.util.Locale;

public class EnumsDemo {

    enum Gender {
        Male ,Female
    }

    static  enum Days {
        Saturday(1,"Sat"),
        Sunday(2,"Sun"),
        Monday(3,"Mon"),
        Tuesday(4,"Tue"),
        Wednesday(5,"Wed"),
        Thursday(6,"Thu"),
        Friday(7,"Fri");

        private int index;
        private String abbreviation;

        public int getIndex() {
            return index;
        }
        public String getAbbreviation() {
            return abbreviation;
        }
        Days(int i, String abv) {
            index = i;
            abbreviation = abv;
        }
    }
    static  class Human
    {
        public Gender gender ;
    }


    public static void main(String[] args) {

        Human h = new Human();
        h.gender= Gender.Female ;

        System.out.println("Days.Monday = " + Days.Monday);
        System.out.println("Days.Monday = " + Days.valueOf("Monday"));
        System.out.println("Days.Monday.ordinal() = " + Days.Monday.ordinal());

        System.out.println(Days.Monday.getIndex());
        System.out.println(Days.Monday.getAbbreviation());

        for(Days day : Days.values())
            System.out.printf(Locale.getDefault(),"%-3d, %-3d, %-10s ,%-5s %n",day.ordinal(),day.getIndex(),day.name(),day.getAbbreviation());
    }
}
