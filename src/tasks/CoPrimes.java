package tasks;

import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;

public class CoPrimes {
    // TODO: 1) the user is prompted to enter a positive integer n, 0 < n <= 10
    // TODO: 2) Promote the user to enter n integer number
    // TODO: 3) test if all the n numbers are co-primes  , co-prime numbers must have gcd(x,y) = 1 for all x, y in the n numbers

    public static void main(String[] args) {
        int a1 = 0, a2 = 0, a3 = 0, a4 = 0, a5 = 0, a6 = 0, a7 = 0, a8 = 0, a9 = 0, a10 = 0;

        Scanner scanner = new Scanner(System.in);
        System.out.print("N:");
        int n = scanner.nextInt();          /// 5
        if (n <= 0 || n > 10) {
            System.out.println("Invalid n value:" + n);
        } else {
            for (int i = 1; i <= n; i++) {      // 1
                System.out.printf(Locale.getDefault(), "The integer number [%d]:", i);
                switch (i) {
                    case 1:
                        a1 = scanner.nextInt();
                        break;
                    case 2:
                        a2 = scanner.nextInt();
                        break;
                    case 3:
                        a3 = scanner.nextInt();
                        break;
                    case 4:
                        a4 = scanner.nextInt();
                        break;
                    case 5:
                        a5 = scanner.nextInt();
                        break;
                    case 6:
                        a6 = scanner.nextInt();
                        break;
                    case 7:
                        a7 = scanner.nextInt();
                        break;
                    case 8:
                        a8 = scanner.nextInt();
                        break;
                    case 9:
                        a9 = scanner.nextInt();
                        break;
                    case 10:
                        a10 = scanner.nextInt();
                        break;
                }
            }

            int max = 0, min = 0, gcd = 1;
            boolean isCoPrime = true;
            System.out.printf(Locale.getDefault(), "The integer number are:");
            for (int i = 1; i <= n; i++) {
                switch (i) {
                    case 1:
                        System.out.printf(Locale.getDefault(), "%5d, ", a1);
                        isCoPrime = true;
                        break;
                    case 2:
                        System.out.printf(Locale.getDefault(), "%5d, ", a2);
                        // you can get the gcd using this
                        // gcd = BigInteger.valueOf(a1).gcd(BigInteger.valueOf(a2)).intValue();
                        max = a1 > a2 ? a1 : a2;
                        min = a1 > a2 ? a2 : a1;
                        for (int j = min; j >= 1; --j) {
                            if (max % j == 0 && min % j == 0) {
                                gcd = j;
                                break;
                            }
                        }
                        isCoPrime = (gcd == 1);
                        if (!isCoPrime)
                            i = n;              // to stop the loop
                        break;
                    case 3:
                        System.out.printf(Locale.getDefault(), "%5d, ", a3);
                        max = a1 > a3 ? a1 : a3;
                        min = a1 > a3 ? a3 : a1;
                        for (int j = min; j >= 1; --j) {
                            if (max % j == 0 && min % j == 0) {
                                gcd = j;
                                break;
                            }
                        }
                        isCoPrime = (gcd == 1);
                        if (!isCoPrime) {
                            i = n;              // to stop the loop
                            break;
                        }
                        max = a2 > a3 ? a2 : a3;
                        min = a2 > a3 ? a3 : a2;
                        for (int j = min; j >= 1; --j) {
                            if (max % j == 0 && min % j == 0) {
                                gcd = j;
                                break;
                            }
                        }
                        isCoPrime = (gcd == 1);
                        if (!isCoPrime)
                            i = n;              // to stop the loop

                        break;
                    // TODO: complete all cases
                    case 4:
                        System.out.printf(Locale.getDefault(), "%5d, ", a4);
                        // TODO: find gcd between (a1 and a4) ,  (a2 and a4) , (a3 and a4)
                        break;
                    case 5:
                        System.out.printf(Locale.getDefault(), "%5d, ", a5);
                        // TODO: find gcd between (a1 and a5) ,  (a2 and a5) , (a3 and a5) , (a4 and a5)
                        break;
                    case 6:
                        System.out.printf(Locale.getDefault(), "%5d, ", a6);
                        // TODO: find gcd between (a1 and a6) ,  (a2 and a6) , (a3 and a6) , (a4 and a6) , (a5 and a6)
                        break;
                    case 7:
                        System.out.printf(Locale.getDefault(), "%5d, ", a7);
                        // TODO: find gcd between (a1 and a7) ,  (a2 and a7) , (a3 and a7) , (a4 and a7) , (a5 and a7) ,(a6 and a7)
                        break;
                    case 8:
                        System.out.printf(Locale.getDefault(), "%5d, ", a8);
                        // TODO: find gcd between (a1 and a8) ,  (a2 and a8) , (a3 and a8) , (a4 and a8) , (a5 and a8) ,(a6 and a8) ,(a7 and a8)
                        break;
                    case 9:
                        System.out.printf(Locale.getDefault(), "%5d, ", a9);
                        // TODO: find gcd between (a1 and a9) ,  (a2 and a9) , (a3 and a9) , (a4 and a9) , (a5 and a9) ,(a6 and a9) ,(a7 and a9) ,(a8 and a9)
                        break;
                    case 10:
                        System.out.printf(Locale.getDefault(), "%5d, ", a10);
                        // TODO: find gcd between (a1 and a10) ,  (a2 and a10) , (a3 and a10) , (a4 and a10) , (a5 and a10) ,(a6 and a10) ,(a7 and a10) ,(a8 and a10) ,(a9 and a10)
                        break;
                }
            }

            if (isCoPrime)
                System.out.println(", this group are co-primes");
            else
                System.out.println(", this group are NOT co-primes");
            System.out.println("\b\b\n");
        }


        scanner.close();
    }


}
