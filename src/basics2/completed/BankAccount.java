package basics2.completed;

import java.util.Currency;

public class BankAccount {

    private int id ;
    private  String OwnerName ;
    private Money balance ;
    private Country country ;

    private class Money2 {
        private double Amount;
        private Currency currency;

        public double getAmount() {
            return Amount;
        }

        public Money2(double amount) {
            Amount = amount;
            switch (country) {
                case Syria:
                    currency = null;             // put the jordan currency
                    break;
                case Jordan:
                    currency = null;            // put the jordan currency
                    break;
            }
        }

        public void setAmount(double amount) {
            Amount = amount;
        }

        public Currency getCurrency() {
            return currency;
        }
    }



    private static class  Money {
        private double Amount ;
        private Currency currency ;

        public double getAmount() {
            return Amount;
        }

        public void setAmount(double amount) {
            Amount = amount;
        }

        public Currency getCurrency() {
            return currency;
        }

        public void setCurrency(Currency currency) {
            this.currency = currency;
        }
    }
}
