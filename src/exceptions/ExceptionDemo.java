package exceptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class ExceptionDemo {
    public static int divide(int a, int b) {
//        int temp  ;
//        if(b != 0)
//            return  a/b ;
//        else
//            return 0 ;
//        try {
//            int temp = a / b;
//            return temp;
//        }catch (Exception ex)
//        {
//            System.err.println("Exception :"+ex.getMessage());
//        }

        int temp = a / b;
            return temp;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = 0, y = 0;
        String input;
        do {


//            if (y != 0)
//                System.out.printf(Locale.getDefault(), "%3d / %3d = %5d%n", x, y, divide(x, y));
//            else
//                System.out.printf(Locale.getDefault(), "%3d / %3d = %5s%n", x, y, '\u221E');


           // File file = new File("");
           // BufferedReader reader = new BufferedReader(new FileReader(file));
            try{

                System.out.print("X:");
                x = scanner.nextInt();      // 3
                System.out.print("Y:");
                y = scanner.nextInt();      // 3
                scanner.nextLine();
                System.out.printf(Locale.getDefault(), "%3d / %3d = %5d%n", x, y, divide(x, y));    // 1

            }catch (ArithmeticException e)
            {
                System.err.println("ArithmeticException :"+e.getMessage());
                System.out.printf(Locale.getDefault(), "%3d / %3d = %5s%n", x, y, '\u221E');
            }catch (InputMismatchException ex)
            {
                System.err.println("InputMismatchException :"+ex.getMessage()+ ", please try again.");
            }catch (Error er)
            {
                System.err.println("Some error :"+er.getMessage());
            } catch (Exception e)
            {
                System.err.println("Some Exception :"+e.getMessage());
            }catch (Throwable t)
            {
                System.err.println("Some Throwable :"+t.getMessage());
            }
            System.out.println("Press any key to continue or q to exit.");
            input = scanner.nextLine().trim().toUpperCase();
            System.out.print("\r\b");
        } while (input == null || input.length() == 0 || input.charAt(0) != 'Q');

        System.out.println("Thank you!");
        scanner.close();


    }
}
