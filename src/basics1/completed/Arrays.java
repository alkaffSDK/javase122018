package basics1.completed;

import java.awt.*;
import java.util.Locale;
import java.util.Random;

public class Arrays {


    // Array: series of adjacent memory locations with the same type

    public static void main(String[] args) {

        int[] arr = {17, 20, 5};
        int[] arr1 = new int[50];


        Random random = new Random();
        System.out.printf(Locale.getDefault(), "%3d, %3d, %3d%n", arr[0], arr[1], arr[2]);

        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = 35 + random.nextInt(66);
            if (arr1[i] > max)
                max = arr1[i];
            System.out.printf(Locale.getDefault(), "%3d, ", arr1[i]);
            if ((i + 1) % 25 == 0) System.out.println();
        }
        System.out.printf(Locale.getDefault(), "max = %d:%d%n", max, arr1[max]);

//        arr[-1] = 2 ;
//        arr[3] = 2 ;
    }
}
