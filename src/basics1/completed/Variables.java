package basics1.completed;


public class Variables {

    // variables : is name for a reference of memory location 
    // Syntax : to define a variable 
    //       [specifier] [modifier]   <DATA TYPE> <variable_id> [=<expression>][,<variable_id> [=<expression>]]*;


    // Filed variable: defined directly  inside a class
    // Filed variable are automatically initialized to its default value
      int field ;           //  int field = 0 ;


    public static void main(String[] args) {

        // local variable : defended inside  a method or (block )
        // local variable must be initialized before using it,
        int local  ;
        local = 10 ;
        System.out.println("local = " + local);

        int x ;
        int a = 10 ;
        int b = a + 20 ;
        int c = 2, d  ;
        int e , f = 3 , g =5;

        System.out.println("a = " + a);
        
    }
}
