package basics1.completed;

public class ClassObjectAndVariable {

    // class : is a description/template/blueprint for a data type that describe its stats (fields) and behaviours (methods)
    // object : is an instance of a class
    // variable : a named reference for a memory location
    // Object class : is a java class that all classes in Java inherited from directly or indirectly

    public static void main(String[] args) {
        int r ;     // r  is a variable
        Mansaf m ;  // is a a variable of type Mansaf
        new Mansaf();       // this will create an object of type Mansaf, but not accessible
        m = new Mansaf();   //this will create a new object of type Mansaf referred by the variable m
        m = new Mansaf();
        Object o = new Object();    // this will create a variable of type Object (Object class) that refers to an object of type Object (Object class)
        Object[] arr = new Mansaf[10];  //In this line: variables: 11, objects: 1
        for(int i=0;i<arr.length;i++)
            arr[i] = new Mansaf();

        //In this line: variables: 11, objects: 11

       // MoreInMethods.staticHi();
    }
}
