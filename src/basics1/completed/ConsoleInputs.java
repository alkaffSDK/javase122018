package basics1.completed;


import java.util.Locale;
import java.util.Scanner;

public class ConsoleInputs {

    public static void main(String[] args) {
        Scanner scanner = new Scanner("start 15 25 this is the line \n 125.32");
        scanner.useLocale(Locale.US);
        int num1 ;
        if(scanner.hasNextInt())
            num1 = scanner.nextInt() ;
        else {
            num1 = 0;
            scanner.next();
        }
        System.out.println("num1 = " + num1);
        num1 = scanner.nextInt() ;
        System.out.println("num1 = " + num1);
        //scanner.nextLine() ;
        String str = scanner.nextLine() ;
        System.out.println("str = \"" + str+"\"");


        System.out.print("Mark:");
        double mark = scanner.nextDouble();

        System.out.println("mark = " + mark);

        scanner.close();
    }
}
