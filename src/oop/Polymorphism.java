package oop;

import java.util.Locale;
import java.util.Random;

public class Polymorphism {

    interface Flyable {
        int x = 10;             // All variable in interface are public static final

        // All methods in interfaces are by default public abstract
        // abstract method: is a method without any body (implementation)
        String canFly();        // must be overridden by any concrete sub class (non abstract class )
        //

        // in interface you can add implementation for some method

        // 1) default methods : I could not be overridden

        default void defaultMethod()
        {
            System.out.println("This is a default method");
            privateMethod();        // you can call private methods just from inside the  default or private methods in the interface
        }

        // 2) static methods : I could not  inherited

        static void staticMethod()
        {
            System.out.println("Static method");
            // privateMethod();
        }
        // 3) private methods : I could not  overridden (In JAVA 9 or above)
        private void privateMethod()
        {
            System.out.println("Private Method");
        }

    }


    static class Plane extends Vehicle implements Flyable {

        @Override
        public String canFly() {
            return null;
        }
    }

    abstract static class Animal {

        abstract  public String printTypename() ;
//        {
//            return "Animal";
//        }
    }

    static abstract class Mammal extends Animal {
        //public String printTypename();

//        {
//            return "Mammal";
//        }

        public String justInMammal() {
            return "justInMammal";
        }
    }

    static abstract class Bird extends Animal {
//        public String printTypename() {
//            return "Bird";
//        }
    }

    // you can implements as many interfacees as you need
    static class Falcon extends Bird implements Flyable {
        public String printTypename() {
            return "Falcon";
        }

        @Override
        public String canFly() {
            return ", it can fly";
        }

    }

    static class Penguin extends Bird {
        public String printTypename() {
            return "Penguin";
        }
    }

    static class Bat extends Mammal implements Flyable {
        public String printTypename() {
            return "Bat";
        }

        public String canFly() {
            return ", it can fly";
        }

    }

    static class Cat extends Mammal {
        public String printTypename() {
            return "Cat";
        }
    }

    static class Eagle extends Bird implements Flyable {
        public String printTypename() {
            return "Eagle";
        }

        @Override
        public String canFly() {
            return ", it can fly";
        }
    }

    public static void main(String[] args) {

//        Mammal mammal = new Mammal();
//        Bird bird = new Bird();
//
//        Animal animal0 = new Animal();
//        Animal animal1 = new Mammal();
//        Animal animal2 = new Bird();

        // animal1.justInMammal();
//        ((Mammal) animal1).justInMammal();
//        System.out.println(animal0.printTypename());        // Animal
//        System.out.println(animal1.printTypename());        // Mammal
//        System.out.println(animal2.printTypename());        // Bird

        Random random = new Random();
        Animal[] animals = new Animal[20];

        for (int i = 0; i < animals.length; i++) {
            switch (random.nextInt(5)) {
                case 0:
                    animals[i] = new Cat();
                    break;
                case 1:
                    animals[i] = new Penguin();
                    break;
                case 2:
                    animals[i] = new Falcon();
                    break;
                case 3:
                    animals[i] = new Bat();
                    break;
                case 4:
                    animals[i] = new Eagle();
                    break;
//                case 4:
//                    animals[i] = new Mammal();
//                    break;
//                case 5:
//                    animals[i] = new Bird();
//                    break;
//                case 6:
//                    animals[i] = new Eagle();
//                    break;
//                default:
//                    animals[i] = new Animal();
            }

            System.out.printf(Locale.getDefault(), "%3d) %s", i + 1, animals[i].printTypename());
            // System.out.println(((Falcon) (animals[i])).canFly()); // casting error

//            if(animals[i] instanceof Falcon)
//                System.out.println(((Falcon) (animals[i])).canFly());
//            else if( animals[i] instanceof  Bat)
//                System.out.println(((Bat) (animals[i])).canFly());
//            else
//                System.out.println();


            if (animals[i] instanceof Flyable)
                System.out.println(((Flyable) (animals[i])).canFly());
            else
                System.out.println();

        }


    }
}
