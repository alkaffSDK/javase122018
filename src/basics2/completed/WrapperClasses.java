package basics2.completed;

import java.util.Locale;

public class WrapperClasses {

    public static void main(String[] args) {

        // primitive
        int x = 10;


        Integer i = 10;            // this automatic boxing
        x = i;                     // this an automatic unboxing

        Integer ii = new Integer(10);
        // immutable
        String str = "Abc";
        String str1 = new String("Abc");

        // reference
        Object o = new Object();

        add(x, x);       // 1
        add(x, i);       // 2
        add(i, x);       // 3
        add(i, i);       // 4


        int a = 127;
        Integer c = a;
        Integer d = a;

        System.out.println(c == d);     // true

        System.out.println(c.equals(d));
        a = 128;
        c = a;
        d = a;

        System.out.println(c == d);     // true
        System.out.println(c.equals(d));
        System.out.println("Integer.reverse(a) = " + Integer.reverse(a));


        System.out.printf(Locale.getDefault(), "a          = %s%n", String.format("%32s", Integer.toBinaryString(a)).replaceAll(" ", "0"));
        System.out.printf(Locale.getDefault(), "reverse(a) = %s%n", String.format("%32s", Integer.toBinaryString(Integer.reverse(a))).replaceAll(" ", "0"));

    }

    private static void add(int a, int b) {
        System.out.println("1");
    }

    private static void add(int a, Integer b) {
        System.out.println("2");
    }

    private static void add(Integer a, int b) {
        System.out.println("3");
    }

    private static void add(Integer a, Integer b) {
        System.out.println("4");
    }
}
