package generics;

import basics1.Person;

public class genericsDemo {

    static class  Pair {
        // Key
        private  Object Key;
        // Value
        private   Object Value ;

        public Object getKey() {
            return Key;
        }

        public void setKey(Object key) {
            Key = key;
        }

        public Object getValue() {
            return Value;
        }

        public void setValue(Object value) {
            Value = value;
        }
    }

    static class  GenericPair<K,V> {
        // Key
        private  K Key;
        // Value
        private   V Value ;

        public K getKey() {
            return Key;
        }

        public void setKey(K key) {
            Key = key;
        }

        public V getValue() {
            return Value;
        }

        public void setValue(V value) {
            Value = value;
        }
    }


    public static void main(String[] args) {

        Pair p1 = new Pair();
        p1.setKey(1);
        p1.setValue("String");


        Pair p2 = new Pair();
        p2.setKey("string");
        p2.setValue(new Person());


        GenericPair<Integer,String> gp1 = new GenericPair<>();
        gp1.setKey(1);
        gp1.setValue("String");


        GenericPair<String,Person> gp2 = new GenericPair<>();
        gp2.setKey("string");
        gp2.setValue(new Person());


    }


}
