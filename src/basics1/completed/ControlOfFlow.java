package basics1.completed;

public class ControlOfFlow {
    /**
     * 1)if
     * syntax:
     * if(<boolean_expression>)
     * statement or block {}
     * [else statement or block {} ]
     * 2) switch
     * syntax:
     * switch(<switch_variable>)
     * {
     *  [case <unique_constant_value>:
     *  [statement]* ]*
     * [default :
     * [statement]*]
     * }
     *
     * <switch_variable> : integer , short, byte, String, boolean, char, and enum
     * 3) ternary operator (if-else-then)
     * syntax :
     *              <boolean_expression> ? <value_if_true> : <value_if_false>
     */

    public static void main(String[] args) {

        int a = 15;
        if (a < 10) {
            System.out.println("Yes");
            ;
        } else
            System.out.println("No");
        System.out.println("Done");

        String str = a <10 ? "Yes" : "No" ;
        System.out.println(str);

        System.out.println(a <10 ? "Yes" : "No");
        // Error : a <10 ? "Yes" : "No"
        //Error a <10 ?  System.out.println("Yes") :  System.out.println("No") ;
        final int s = 5 ;
        int d = 15;        // 0.325649999999996
        // switch test the cases first (in order) then goes to the default if exist regardless the order of the default and cases
        switch (d) {
            case 0:
            case s-1:
                System.out.println("Zero");
                break;
            case 1:
            case 6:
                System.out.println("One");
                break;
            default:
                System.out.println("Default");
        }
//


        double dob = 153.32565;
        int c1 = (int) dob;
        double c2 = dob - c1;
        System.out.println("c1 = " + c1);       // 153
        System.out.println("c2 = " + c2);       // 0.32565

        
    }
}
