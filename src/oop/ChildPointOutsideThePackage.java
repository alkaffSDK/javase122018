package oop;

import oop.clasess.Point;

public class ChildPointOutsideThePackage extends  Point {

    public  void method()
    {
        Point p = new Point();
         // System.out.println("Private = " + p.Private);   // not accessible
         // System.out.println("Package = " + p.Package);     // not accessible
         // System.out.println("Protected = " + p.Protected);  // not accessible because P is not the parent object of this object (the object that you call the method through)
        System.out.println("Protected = " + Protected); // this is the Protected  variable that can be acceptable from the child object
        System.out.println("Public = " + p.Public);
    }
}
