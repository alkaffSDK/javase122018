package exceptions;

import java.io.*;
import java.util.Scanner;

import static exceptions.ExceptionDemo2.*;

public class ExceptionDemo3 {
    public static void main(String[] args) {

        String filepath = "D:\\Test.txt";
        readFromFile(null);
    }

    private static void readFromFile(String filepath) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(new File(filepath)));

            // to read the file (will be discussed later)
            reader.lines().forEach(System.out::println);


        } catch (FileNotFoundException e) {
            System.err.println("File or directory are not found or unacceptable");
            //e.printStackTrace();
        } catch (NullPointerException ne) {
            System.err.println("File name with null value");
            //ne.printStackTrace();
        } catch (IOException ne) {
            System.err.println("can't read teh file:" + filepath);
            throw ne;
            //ne.printStackTrace();
        } finally {

            // finally block  execution is guaranteed if the try block is entered
            try {
                if (reader != null)
                    reader.close();

            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }

    private static void readFromFileBetterCode(String filepath) {
        // Starting form Java 7

        // this resource will be closed automatically what ever happened after the try
        try (BufferedReader reader =new BufferedReader(new FileReader(new File(filepath)));
             Scanner scanner = new Scanner(System.in);
             Person o = new Person())       // the person class must implements AutoClosable interface and override close method in order to be used in try with resources
        {
            // to read the file (will be discussed later)
            reader.lines().forEach(System.out::println);
        }catch (FileNotFoundException e) {
            System.err.println("File or directory are not found or unacceptable");
            //e.printStackTrace();
        } catch (NullPointerException ne) {
            System.err.println("File name with null value");
            //ne.printStackTrace();
        } catch (IOException ne) {
            System.err.println("can't read teh file:" + filepath);
           //ne.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
