package basics1.completed;

import java.util.Calendar;
import java.util.Locale;

public class ConsoleOutput {

    public static void main(String[] args) {

        System.out.println("with new line");        // cout<<"basics1.completed.Hello"<<endl;
        System.out.print("with no new line");          // cout<<"basics1.completed.Hello";
        System.out.println(10);
        System.out.println(10 + 20);
        System.out.println(10 + 20 + 5);

        System.out.println("--------------");
        System.out.println("10" + 20 + 5);      // 10205
        System.out.println("10" + (20 + 5));    // 1025
        System.out.println(10 + "20" + 5);      // 10205
        System.out.println(10 + 20 + "5");      // 305

        System.out.println("Gold " + 26.32546);
        System.out.println("Silver " + 2.3);
        System.out.println("Bronze " + 1.64);

        System.out.printf(Locale.getDefault(),"%s : %f%n","Gold",26.32546);
        System.out.printf(Locale.getDefault(),"%s : %f%n","Silver",2.3);
        System.out.printf(Locale.getDefault(),"%s : %f%n","Bronze",1.64);
        System.out.println("-----------------------------");
        System.out.printf(Locale.getDefault(),"%10s : %f%n","Gold",26.32546);
        System.out.printf(Locale.getDefault(),"%10s : %f%n","Silver",2.3);
        System.out.printf(Locale.getDefault(),"%10s : %f%n","Bronze",1.64);
        System.out.println("-----------------------------");
        System.out.printf(Locale.getDefault(),"%-10s : %f%n","Gold",26.32546);
        System.out.printf(Locale.getDefault(),"%-10s : %f%n","Silver",2.3);
        System.out.printf(Locale.getDefault(),"%-10s : %f%n","Bronze",1.64);
        System.out.println("-----------------------------");
        System.out.printf(Locale.getDefault(),"%-10s:%10.2f%n","Gold",2254786.32546);
        System.out.printf(Locale.getDefault(),"%-10s:%010.2f%n","Silver",2.3);
        System.out.printf(Locale.getDefault(),"%-10s:%010.2f%n","Bronze",1.64);
        System.out.println("-----------------------------");


        System.out.printf(Locale.getDefault(),"%-15s: %8d %n","Integer",50);
        System.out.printf(Locale.getDefault(),"%-15s: %-8d %n","Integer",50);
        System.out.printf(Locale.getDefault(),"%-15s: %08d %n","Integer",50);
        System.out.printf(Locale.getDefault(),"%-15s: %+08d %n","Integer",50);
        System.out.printf(Locale.getDefault(),"%-15s: %+08d %n","Integer",-50);
        System.out.printf(Locale.getDefault(),"%-15s: %+-8d %n","Integer",-50);


        System.out.printf(Locale.getDefault(),"%-15s: %d%% %n","Percentage",50);
        System.out.printf(Locale.getDefault(),"%-15s: %x %n","hexadecimal",50);
        System.out.printf(Locale.getDefault(),"%-15s: %e %n","E notation",50.125455);
        System.out.printf(Locale.getDefault(),"%-15s: %8.2e %n","E notation",50.125455);
        System.out.printf(Locale.getDefault(),"%-15s: %4.2e %n","E notation",50.125455);
        System.out.printf(Locale.getDefault(),"%-15s: %10.2e %n","E notation",50.125455);
        System.out.printf(Locale.getDefault(),"%-15s: %x %n","hexadecimal",50);
        System.out.printf(Locale.getDefault(),"%-15s: %o %n","Octal",50);
        System.out.printf(Locale.getDefault(),"%-15s: %tH %n","Time hour", Calendar.getInstance().getTime());
        System.out.printf(Locale.getDefault(),"%-15s: %tM %n","Time minutes", Calendar.getInstance().getTime());

        System.out.println("Hello\tthere");
        System.out.println("Hello\bthere");
        System.out.println("Hello\b\b\b\b");
        System.out.println("Hello\nthere");
        System.out.println("Hello\rh");
        System.out.print("Hello\rh");
        System.out.println("Hello\fthere");
        System.out.println("Hello\\there");
        System.out.println("\"Hello there\"");
        System.out.println("\'Hello there\'");
        System.out.println("E:\\Adobe 2");


    }
}

