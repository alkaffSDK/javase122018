package basics1.completed;


public class ClassesAndMethods {

    // RULE : ALL CODES IN JAVA MUST BE WRITTEN INSIDE A CLASS


    // Method: it a named block of code with return data type and [optional] inputs (parameters)

    // Syntax :
    //  1) to define a method  [modifier] [specifier] <RETURN_DATA_TYPE> <METHOD_NAME> ([Parameter list]) { [statement]* }
    //  2) to call a method  [(variable|class).]<METHOD_NAME>([Argument list]);


    // All methods must be written inside a class
    {
        // this is a block
    }

    void foo1() {

    }
    void foo2(int a) {
        return ;    // return will terminate the method execution
        //System.out.println();
    }
    int foo3() {

        // All execution paths  must ends with return if the method has a non void return data type
        if(1== 5) {
            System.out.println("");
            return 0;
        }

        return 5 ;

    }


    public static void main(String[] args) {

    }


}
