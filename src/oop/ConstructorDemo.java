package oop;

public class ConstructorDemo {



    // Constructor : is a special method, that must  has the same class name with no return data type
    // Constructor is called after create each object
    // If the developer doesn't implement a constructor,
    // then the compiler will create a default constructor with empty parameters

    // Constructors can be overloaded but not overridden or inherited

    static class Vehicle {

        // The first statement of any constructor is call for another constructor !!!!!!!!!!!!!
        // this call for a constructor could be one of two cases
        // 1) call for a constructor from the super (parent) class [default]
        //    How : super();    this is a call for a constructor from the super (parent) class
        // 2) call for a constructor from the SAME  class
        //   How : this();    this is a call for a constructor from the SAME  class

        // at least on of the constructor must call the parent class constructor
        public  Vehicle(int a)
        {
            this();
            //super();            // if you did not add this statement at the begging of the body in the constrictor, then the compiler will add it automatically
            System.out.println("Vehicle(int)");


        }

        public  Vehicle()
        {
            super();
            System.out.println("Vehicle()");
        }

        // this is a method not a constructor, because it has a return data type
        public  void Vehicle()
        {
            System.out.println("this is a Method, NOT a constructor");
        }
    }


    static class Car extends Vehicle {

        public Car ()
        {
            this(0.0);
            System.out.println("Car()");
        }
        public Car (int a)
        {
            super(a);
            System.out.println("Car(int)");
        }

        public Car (double d)
        {
            this(1);
            System.out.println("Car(double)");
        }
    }
    public static void main(String[] args) {

//        Vehicle v  = new Vehicle();
//        // the above line will create a variable v of type Vehicle that will refer to a new object of type Vehicle,
//        // before create that object a constructor must be called
//        Vehicle v1 = new Vehicle(10);
//        v.Vehicle();
        // this next line will create an object of type car, but first it must create an object of type Vehicle
        // and before that an object of type Object class must be created.
        Car c = new Car();
    }
}
