package oop;

import oop.clasess.Point;

public class OutSideThePackage {

    public  void method()
    {
        Point p = new Point();
        // System.out.println("Private = " + p.Private);   // not accessible
        // System.out.println("Package = " + p.Package);     // not accessible
        // System.out.println("Protected = " + p.Protected);  // not accessible
        System.out.println("Public = " + p.Public);
    }
}
