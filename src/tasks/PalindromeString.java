package tasks;

import java.util.Scanner;

public class PalindromeString {
    // TODO: Test if the string is palindrome or not, ignore spaces at the start and end of the string and the case differences
    /* Examples :
                    "a" -> yes
                    " a" -> yes
                     "a " -> yes
                    "ab" -> no
                    "aba" -> yes
                    "aba " -> yes
                    " aba" -> yes
                    " abA" -> yes
                    "abbaba" -> no
                    "abCDEdCbA" -> yes
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char ch;
        int lastIndex, mid;
        boolean isPalindrome;
        String original, str;
        do {
            // ٌread input string
            System.out.print("Input:");
            original = scanner.nextLine();
            // Remove the spaces  and change all upper cases characters to lowercase
            str = original.trim().toLowerCase();

            // Solution 1 : faster
            isPalindrome = true;
            lastIndex = str.length() - 1;
            mid = str.length() / 2;
            for (int i = 0; i < mid; i++) {
                if (str.charAt(i) != str.charAt(lastIndex - i)) {
                    isPalindrome = false;
                    break;
                }
            }
            // Solution 2 : Slower
            StringBuilder builder = new StringBuilder(str);
            isPalindrome = str.equalsIgnoreCase(builder.reverse().toString());

            System.out.printf("The string \"%s\" is %s palindrome%n", original, isPalindrome ? "\b" : "not");
            System.out.println("Press enter to continue of Q to exit");

            str = scanner.nextLine().trim();
            ch = str.length() > 0 ? str.charAt(0) : '*';
        } while (ch != 'q' && ch != 'Q');
        System.out.println("Done");
        scanner.close();
    }
}
