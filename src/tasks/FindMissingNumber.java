package tasks;

import javax.security.sasl.SaslClient;
import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;

public class FindMissingNumber {
    // TODO: 1) write code to find the missing number from the numbers in data.txt
    // TODO: 2) Write a code to print the repeated numbers from the numbers in data.txt


    // TODO: 3) rewrite this solution using methods


    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("D:\\Java Workspace\\JavaSE122018\\src\\tasks\\data.txt"));

        // 8366 is the maximum value in all numbers
        byte[] counter = new byte[8367];
        int value;
        // to separate the file by ',' or ' ' or new line '\n'
        scanner.useDelimiter(",|\n| ");
        String token;
        while (scanner.hasNext()) {
            token = scanner.next();
            // don't print any non digit or empty token
            if (token.length() > 0 && Character.isDigit(token.charAt(0))) {
                value = Integer.valueOf(token);
                counter[value]++;
            }
        }
        System.out.print("The missing number(s) is/are:");
        for (int i = 0; i < counter.length; i++)
            if (counter[i] == 0)
                System.out.print(i + ", ");
        System.out.print("\b\b\n");

        int c = 0;
        System.out.println("The repeated number(s) is/are;  number:(Frequency):");
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] > 1) {
                System.out.printf(Locale.getDefault(), "%5d:(%d), ", i, counter[i]);
                if ((++c % 15) == 0)
                    System.out.println();
            }
        }
        System.out.print("\b\b\n");
        // OR

//        for(int i=0;i<counter.length;i++)
//        {
//            if(counter[i] == 0)
//                System.out.printf(Locale.getDefault(),"The number (%d) is missing%n" ,i);
//            else if (counter[i] > 1)
//                System.out.printf(Locale.getDefault(), "The number (%d) is repeated %d times %n" ,i,counter[i]);
//
//        }
        scanner.close();

    }


}
