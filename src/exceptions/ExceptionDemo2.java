package exceptions;

public class ExceptionDemo2 {
    static class Person implements AutoCloseable {
        private String Name;
        private int Age ;

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public int getAge() {
            return Age;
        }

        public void setAge(int age) throws  Exception {
            if(age >0 && age <= 150)
                Age = age;
            throw  new Exception("Invalid age value, age must be from 1 to 150, current age:"+age);
        }

        @Override
        public String toString() {
            return "Person{" +
                    "Name='" + Name + '\'' +
                    ", Age=" + Age +
                    '}';
        }

        @Override
        public void close() throws Exception {

        }

//        @Override
//        public void close() throws Exception {
//
//        }
    }

    public static void main(String[] args) {
            
        Person p = new Person();

        try {
            p.setAge(500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("p = " + p);
        
    }
    
}
