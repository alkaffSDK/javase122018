package oop.clasess;

public class InTheSamePakcge {

    public  void method()
    {
        Point p = new Point();
        // System.out.println("Private = " + p.Private);   // not accessible
        System.out.println("Package = " + p.Package);
        System.out.println("Protected = " + p.Protected);
        System.out.println("Public = " + p.Public);
    }
}
