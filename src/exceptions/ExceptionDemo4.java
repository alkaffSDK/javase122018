package exceptions;

import java.io.IOException;
import java.util.Random;

public class ExceptionDemo4 {

    public static void main(String[] args) {


        int number;
        // TODO : What is the output if the  number is 0, 1, 2, 3, and  4
        //
        try {
            Random random = new Random();
            number = random.nextInt(5);
            System.out.println("number = " + number);
            System.out.println("A");
            issueSomeExceptions(number);
            System.out.println("B");
        } catch (Exception e) {
            if (e instanceof ArithmeticException) {
                System.out.println("C");
                // TODO: handle this type of exceptions
            } else if (e instanceof IndexOutOfBoundsException) {
                System.out.println("D");
                throw new IllegalArgumentException();
                // TODO: handle this type of exceptions
            } else if (e instanceof IllegalArgumentException) {
                System.out.println("E");
                // TODO: handle this type of exceptions
            } else if (e instanceof IOException) {
                System.out.println("F");
                // TODO: handle this type of exceptions
            } else {
                System.out.println("G");
                // TODO: handle this type of exceptions
            }
        } finally {
            System.out.println("I");
        }
        System.out.println("J");
    }

    private static void issueSomeExceptions(int nextInt) throws Exception {
        switch (nextInt) {
            case 0:
                throw new ArithmeticException();
            case 1:
                throw new IndexOutOfBoundsException();
            case 2:
                throw new IllegalArgumentException();
            case 3:
                throw new IOException();
            default:
                throw new Exception();
        }
    }
}
