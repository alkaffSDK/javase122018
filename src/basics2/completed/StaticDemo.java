package basics2.completed;
import static java.lang.System.out;
public class StaticDemo {

    /// *****    EVERY CODE IN JAVA IS WRITE INSIDE A CLASS, HOWEVER NOT ALL ARE DEFENDED INSIDE IT    ******/
    // static : static keyword used with
    // 1) class variables : it defines the variable in the class itself
    // 2) blocks :  (later in OOP)
    // 3) methods : it defines the method in the class itself
    // 4) inner classes : it defines the class in the class itself (later in inner classes)
    // 5) import :


    int instanceVariable ;          // each object has it's own copy of instanceVariable

    static  int classVariable ;    // static variable : there is only one a variable called classVariable in the class itself
    // and shared by all objects


    public void nonStaticMethod()
    {
        instanceVariable = 10 ;
        classVariable = 20 ;


    }

    public static void staticMethod()
    {
        // Error : instanceVariable = 10 ; instance members are not accessible from static method
        classVariable = 20 ;
    }
    public static void main(String[] args) {

       // Error :  static  int localVariable ;

        StaticDemo obj = new StaticDemo();
        obj.instanceVariable = 10 ;


        obj.classVariable = 10 ;
        StaticDemo.classVariable = 10 ;

        System.out.println();
        // System: is a class
        // out: static variable
        // println(): a method

        // by adding import static java.lang.System.out; you can write  System.out.println() as follow
        out.println();

    }




}
