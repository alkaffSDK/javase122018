package tasks;


import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class InClass {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Random random = new Random();
        System.out.print("Please enter positive number :");
        int n  =scanner.nextInt();

        while (n <= 0)
        {
            System.out.print("N must be positive number, try again :");
             n  =scanner.nextInt();
        }
        int [] arr =new int[n];
        for(int i=0;i<arr.length;i++)
        {
            arr[i] = random.nextInt(101);
            System.out.printf(Locale.getDefault(),"%5d, ",arr[i]);
        }

        int[] minMax = findMinMax(arr);
        System.out.printf(Locale.getDefault(),"The min is :%d, and the max is:%d%n",minMax[0],minMax[0]);
        scanner.close();
    }

    private static int[] findMinMax(int[] arr) {

        int[] minmax = {Integer.MAX_VALUE,Integer.MIN_VALUE};
        for(int i=0;i<arr.length;i++)
        {
            if(arr[i] > minmax[1])
                minmax[1] = arr[i];

            if(arr[i] < minmax[1])
                minmax[0] = arr[i];

        }

        return  minmax ;
    }
}
